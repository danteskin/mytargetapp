class Task < ApplicationRecord
  
  belongs_to :account
  serialize(:names, Array)
end
