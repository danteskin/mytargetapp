class Account < ApplicationRecord
  has_many :adunits, dependent: :destroy
  has_many :account_apps, dependent: :destroy
  has_many :task, dependent: :destroy
end
