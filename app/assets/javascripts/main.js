




// A $( document ).ready() block.



function ready() {
  $('#task_app').on('change', function(e) {
    console.log($(this).attr('data-app-link'));
  });
  $(".js-popup").magnificPopup();
  
  var index = 2;
  $("#testButton").click(function(e){
      e.preventDefault();
      var response_from_new_field;
      var account_id = $(this).attr("data-account-id");
      
      $.ajax({
        url: "/accounts/tasks/new_ad_unit_field",
        type: 'POST',
        data: {account_id: account_id},
        success: function(data){
          console.log(data);
          response_from_new_field = data;
          add_new_field_select();
        }
      });

      function add_new_field_select() {
        // var div = $("#extraFields").appendTo($('#dynamicExtraFieldsContainer'));
        var div = $('<div/>', {
              'class': 'DynamicExtraField'
          }).appendTo($('#extraFields'));
        // var html = '<input type="text" name="task[description_'+index+']"/>';
        var selectField = $('<select class="form-control" name="task[names][]"/>').appendTo(div);
        var optionField;
        

        $.each(response_from_new_field, function(key, option){
          optionField = $('<option value="'+ option.name +'" >'+ option.name +'</option>');
          
          optionField.appendTo(selectField);
          console.log("option create");
        });
        
        
        var removeBtn = $('<button id="removeField">x</button>').appendTo(div);
        console.log("test");
        e.preventDefault();
        // parentDiv.append(html);
        removeBtn.click(function(e){
          e.preventDefault();
          $(this).parent().remove();
        });
      } 

  });
}
$( document ).ready(ready);

  

