class HardWorker
  include Sidekiq::Worker

  def perform(account_id)
    @url = "https://target-sandbox.my.com"
    # login = "buck2192@gmail.com"
    # password = "123qweasd"
    @account = Account.find(account_id)
    @tasks = @account.task.where(status: "не обработано")
    login = @account['login']
    password = @account['password']
    # binding.pry
    MyTargetPadsCreator.new(
      url: @url,
      login: login,
      password: password,
      pads_to_create: @tasks
    ).start
    
  end


end
