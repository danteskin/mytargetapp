class TasksController < ApplicationController

  # skip_before_action :authenticate_admin!, only: [:update_status_from_sidekiq]

  def test
    redirect_to "http://google.ru"
  end
  
  def create

    @account = Account.find(params[:task][:account_id])
    @app = @account.account_apps.find_by_description(params[:task][:app])
    params[:task][:link] = @app.link
    params[:task][:status] = "не обработано"
    my_array_for_names = []
    params[:task][:names].each do |name|
      name = {name: name}
      my_array_for_names << name
    end
    params[:task][:names] = my_array_for_names
    @task = @account.task.new(task_params)

    @task.save
    redirect_to account_path(@account)
    # render plain: params[:task].inspect
  end

  # /tasks/new_ad_unit_field
  def new_ad_unit_field
    @account = Account.find(params[:account_id])
    @ad_unit = @account.adunits.all
    render json: @ad_unit
  end

  def get_tasks_status
    @account = Account.find(params[:account_id])
    tasks = @account.task.all
    render json: tasks.to_json
  end

  def start_sync_with_sidekiq
    # flash[:notice] = "Successfully created..."
    @account = Account.find(params[:account_id])
    account_id = params[:account_id]
    @tasks = @account.task.where(status: "не обработано")
    message_success = {message: "Окей понеслась"}
    message_notice = { message: "нет задач для обработки"}
    tasks = @tasks
    if tasks.size > 0
      HardWorker.perform_async(account_id)
      render json: message_success.to_json
    else
      render json: message_notice.to_json
    end
    # HardWorker.new.perform(account_id)
    # render json: @tasks
  end

  private
  def task_params
    params.require(:task).permit(:app, :link, :status, names: [:name])
  end
end
