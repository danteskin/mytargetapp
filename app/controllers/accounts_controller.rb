class AccountsController < ApplicationController
  def index
    @accounts = Account.all
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @accounts}
      format.json { render :json => @accounts}
    end
  end

  def new    
  end

  def create
    @account = Account.new(account_params)
    @account.save
    redirect_to @account
  end

  def update
    @account = Account.find(params[:id])

    if @account.update(account_params)
      redirect_to @account
    else
      render 'edit'
    end
  end

  def show
    @account = Account.find(params[:id])
  end

  def edit
    @account = Account.find(params[:id])
  end

  def destroy
    @account = Account.find(params[:id])
    @account.destroy
    redirect_to accounts_path
    
  end

  private

  def account_params
    params[:account].permit(:login,:password)    
  end
end
