class Api::Local::TasksController < Api::Local::ApplicationController
  # /api/tasks/update_status PUT
  def update_status_from_sidekiq
    # binding.pry
    account = Account.all

    task = Task.find(params['task_id'])
    new_status = params["status"]  
    task.update(status: new_status)
    render json: account
  end

  # /api/tasks/update_status GET
  def get_status_from_sidekiq
    account = Account.all
    render json: account
  end
end