class AdunitsController < ApplicationController


  def new    
  end

  def create
    @account = Account.find(params[:account_id])
    @adunit = @account.adunits.create(adunit_params)
    redirect_to account_path(@account)
    # render plain: @account.inspect   
  end

  def update
    @adunit = Adunit.find(params[:id])

    if @adunit.update(adunit_params)
      redirect_to @adunit.account
    else
      render 'edit'
    end
  end


  def edit

    @adunit = Adunit.find(params[:id])
    # render plain: params.inspect
  end



  private

  def adunit_params
    params[:adunit].permit(:name)    
  end
end
