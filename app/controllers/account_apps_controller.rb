class AccountAppsController < ApplicationController
  def create
    @account = Account.find(params[:account_id])
    @app = @account.account_apps.create(app_params)
    redirect_to account_path(@account)
  end

  private 

  def app_params
    params[:app].permit(:description, :link)    
  end

end
