class RemoveDescriptionFromAdunits < ActiveRecord::Migration[5.1]
  def change
    remove_column :adunits, :description, :string
  end
end
