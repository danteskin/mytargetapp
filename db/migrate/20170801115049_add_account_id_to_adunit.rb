class AddAccountIdToAdunit < ActiveRecord::Migration[5.1]
  def change
    add_column :adunits, :account_id, :integer
  end
end
