class CreateAccountApps < ActiveRecord::Migration[5.1]
  def change
    create_table :account_apps do |t|
      t.string :description
      t.string :link
      t.integer :account_id

      t.timestamps
    end
  end
end
