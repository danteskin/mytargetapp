class RemoveLinkFromAdunits < ActiveRecord::Migration[5.1]
  def change
    remove_column :adunits, :link, :string
  end
end
