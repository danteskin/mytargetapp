Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  get 'tasks/create'

  match 'accounts/tasks/new_ad_unit_field', to: 'tasks#new_ad_unit_field', via: :post
  match 'accounts/tasks/start', to: 'tasks#start_sync_with_sidekiq', via: :post
  match 'tasks/test', to: 'tasks#test', via: :get
  match 'accounts/tasks/get_tasks_status', to: 'tasks#get_tasks_status', via: :get
  
  # match 'api/tasks/update_status', to: 'api/local/tasks#get_status_from_sidekiq', via: :get

  namespace :api do
    namespace :local do
      match '/update_status', to: 'tasks#update_status_from_sidekiq', via: :patch
    end
  end
  get 'account_apps/create'

  get 'adunit_links/create'

  devise_for :admins, controllers: {
    sessions: 'admins/sessions'
  }

  root 'accounts#index'

  resources :accounts do
    resources :adunits
    resources :account_apps
  end
  


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
